const io = require('socket.io-client');
const rp = require('request-promise');

const options = {
    uri: 'https://www.hltv.org/matches/2341496/sgpro-vs-ldlc-hellcase-cup-8',
    headers: {
        'User-Agent': 'Request-Promise'
    },
};

 rp(options)
    .then( (res) => {
         scorebot()
    })
    .catch( (err) => {
        console.log(err)
    });

function scorebot() {
    const url = 'wss://cf2-scorebot.hltv.org:443';
    const socket = io(url, {
        query: {
            path: '/socket.io',
            transports: ['websocket'],
            origins: 'https://www.hltv.org'
        }
    });

    socket.on("connect", () => {
        console.log('connected');
    });

    socket.on('connect', () => {
        console.log(socket.id);
    });

    socket.on('score', (data) => {
        console.log(data)
    });

    socket.on("disconnect", () => {
        console.log("socket disconnected");
    });

    // error handlers
    socket.on('connect_timeout', (timeout) => {
        console.log(timeout);
    });

    socket.on('connect_error', (error) => {
        console.log(error)
    });

    socket.on('error', (error) => {
        console.log(error)
    });
}


